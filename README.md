<p align="center">
  <img src="./public/dual-colors.png" width="150">
  <h1 align="center">Dual Colors</h1>
  <h4 align="center"><a href="https://alexrintt.github.io/dual-colors"><code>Play in Browser</code></a></h4>
  <p align="center">A creational UI based on a Pinterest idea about palette with two colors</p>
  <p align="center">
    <img src="https://img.shields.io/badge/type-project-orange" alt="Repo Type" />
    <img src="https://img.shields.io/badge/language-typescript-blue" alt="Repo Main Language" />
    <img src="https://img.shields.io/badge/platform-web-orange" alt="Project Platform" />
  </p>
</p>

<p align="center">
  <table>
    <thead>
      <th>
        <img width="450" src="./public/screenshot.png">
      </th>
      <th>
        <img width="450" src="./public/idea.jpg">
      </th>
    </thead>
    <tbody>
      <td>
        <a href="https://alexrintt.github.io/dual-colors">Fig 1. Interface</a>
      </td>
      <td>
        <a href="https://br.pinterest.com/pin/538461699197193701/">Fig 2. Pinterest Idea</a>
      </td>
    </tbody>
  </table>
</p>

## How to use

Just open the [Dual Colors Website](https://alexrintt.github.io/dual-colors) and enjoy.

## Clone the repository for your machine

### Requirements

- Node installed.
- Npm or Yarn installed.

### Installing

1. Clone the repository using the Github client of your choice, or download the repository
   Using the Github client via the command line:

```
git clone https://github.com/alexrintt/dual-colors.git
```

2. Install the dependencies:

```
npm install || yarn install
```

3. Start Parcel Dev Server:

```
npm run dev || yarn dev
```

4. Generate Build

```
npm run build || yarn build
```

## Built with

- [Typescript](https://webpack.js.org/) - Module Bundler.
- [Ethereal Color](https://alexrintt.xyz/ethereal-color) - Color Library.

<br>
<br>
<br>
<br>

<samp>

<h2 align="center">
  Open Source
</h2>
<p align="center">
  <sub>Copyright © 2020-present, Alex Rintt.</sub>
</p>
<p align="center">Dual Colors <a href="https://github.com/alexrintt/dual=colors/blob/master/LICENSE.md">is MIT licensed 💖</a></p>
<p align="center">
  <img src="./public/dual-colors.png" width="35" />
</p>

</samp>
